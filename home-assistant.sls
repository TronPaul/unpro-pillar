home-assistant:
  lookup:
    config:
      homeassistant:
        name: Home
        latitude: 41.9224
        longitude: -87.6524
        elevation: 181
        unit_system: imperial
        time_zone: America/Chicago
        customize:
          media_player.plex_web_firefox:
            hidden: true
          media_player.plex_web_firefox_2:
            hidden: true
          climate.linear_tbz48_thermostat_cooling_1_3_2:
            friendly_name: Thermostat Cooling
          climate.linear_tbz48_thermostat_heating_1_3_1:
            friendly_name: Thermostat Heating
          sensor.linear_tbz48_thermostat_temperature_3_1:
            friendly_name: Thermostat Temperature
      recorder:
        db_url: sqlite:///var/lib/hass/hass.db
      frontend:
      http:
        api_password: '!secret http_password'
      updater:
      discovery:
      conversation:
      history:
      logbook:
      sun:
      sensor:
        platform: yr
      zwave:
        usb_path: /dev/ttyACM0
      climate:
        platform: zwave
