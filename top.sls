base:
  '*':
    - users
  '* and G@virtual:VirtualBox':
    - match: compound
    - sensu
  'domain:ec2.internal':
    - match: grain
    - ec2
  'roles:rabbitmq':
    - match: grain
    - rabbitmq
  'roles:voice_server':
    - match: grain
    - mumble
  'roles:vpn_server':
    - match: grain
    - vpn-server
  'roles:torrent_server':
    - match: grain
    - deluge
  'nasus.chi.teamunpro':
    - nasus_samba
    - nfs
    - home-assistant
  'fednet':
    - nfs
