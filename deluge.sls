deluge:
  config:
    download_location: /srv/torrents/downloading
    move_completed_path: /srv/torrents/completed
    torrentfiles_location: /srv/torrents/torrents
    move_completed: true
    autoadd_location: /srv/torrents/queue
